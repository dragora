# Build recipe for sqlite.
#
# Copyright (c) 2019-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=sqlite
version=3400100
pkgversion=3.40.1
release=1

# Define a category for the output of the package name
pkgcategory=db

tarname=${program}-autoconf-${version}.tar.gz

# Remote source(s)
fetch=https://sqlite.org/2022/$tarname

description="
SQlite is a C-language library that implements a SQL database engine.

The sqlite package is a software library that implements a self-contained,
serverless, zero-configuration, transactional SQL database engine.
"

homepage=https://www.sqlite.org
license="Public Domain"

# Source documentation
docs="README.txt"
docsdir="${docdir}/${program}-${pkgversion}"

# The source has a custom source directory
srcdir=${program}-autoconf-${version}

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" \
    LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(gcc -dumpmachine)" \
     --enable-fts5 \
     --enable-session \
     CPPFLAGS="-DSQLITE_ENABLE_FTS3=1 \
               -DSQLITE_ENABLE_FTS4=1 \
               -DSQLITE_ENABLE_COLUMN_METADATA=1 \
               -DSQLITE_ENABLE_UNLOCK_NOTIFY=1 \
               -DSQLITE_ENABLE_DBSTAT_VTAB=1 \
               -DSQLITE_SECURE_DELETE=1 \
               -DSQLITE_ENABLE_FTS3_TOKENIZER=1"

    make -j${jobs} V=1 && make -j${jobs} check
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Build TCL extension (TEA)

    cd tea

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args --libdir=/usr/lib${libSuffix}

    make -j${jobs} && make -j${jobs} DESTDIR="$destdir" install

    cd ..

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

