# Build recipe for ddrescue.
#
# Copyright (c) 2022 DustDFG, <dfgdust@gmail.com>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=ddrescue
version=1.26
release=2

# Define a category for the output of the package name
pkgcategory=tools

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch=https://ftp.gnu.org/gnu/ddrescue/$tarname

description="
GNU ddrescue is a data recovery tool.

GNU ddrescue copies data from one file or block device (hard disc, cdrom, etc)
to another, trying to rescue the good parts first in case of read errors.
"

homepage=https://www.gnu.org/software/ddrescue/
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/${tarname}"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CXXFLAGS+="$QICXXFLAGS" LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package (if needed)
    if test -d "${destdir}/${infodir}"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/${mandir}"
    then
        (
            cd "${destdir}/${mandir}"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/${docsdir}"
    cp -p $docs "${destdir}/${docsdir}"
}

