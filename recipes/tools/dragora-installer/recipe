# Build recipe for dragora-installer.
#
# Copyright (c) 2019-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=dragora-installer
version=20230410
release=1

# Define a category for the output of the package name
pkgcategory=tools

description="
Installer of Dragora GNU/Linux-Libre.

It contains the installer, help files and parts to perform
a complete installation of Dragora GNU/Linux-Libre.
"

homepage=https://www.dragora.org
license=GPLv3+

build()
{
    # Define package location according to supported architectures
    case $arch in
    amd64 | x32 )
        installer_arch=amd64
        ;;
    i586)
        installer_arch=i586
        ;;
    armhf)
        installer_arch=armhf
        ;;
    *)
        printf '%s\n' "" \
         "The Dragora installer will not be available for the architecture in" \
         "which it is being compiled.  This package will not be produced." \
         "Please contact the developers at <dragora-users@nongnu.org>." \
         "" 1>&2
        sleep 7
        return 0
        ;;
    esac

    mkdir -p "${destdir}"/usr/sbin \
             "${destdir}"/usr/libexec/dragora-installer

    cp -p "${worktree}"/archive/dragora-installer/dragora-installer \
          "${destdir}"/usr/sbin/

    cp -p "${worktree}"/archive/dragora-installer/parts/* \
          "${destdir}"/usr/libexec/dragora-installer/

    for file in "${destdir}"/usr/sbin/* \
                "${destdir}"/usr/libexec/dragora-installer/*
    do
        sed -e "s|@PARTS@|/usr/libexec/dragora-installer/|g" \
            -e "s|@ARCH@|${installer_arch}|g" \
            -i "$file"
    done
    unset -v file

    chmod 755 "${destdir}"/usr/sbin/dragora-installer
    chmod 644 "${destdir}"/usr/libexec/dragora-installer/*

    unset -v installer_arch
}

