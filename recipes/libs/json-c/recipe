# Build recipe for json-c.
#
# Copyright (c) 2022 DustDFG, <dfgdust@gmail.com>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=json-c
version=0.16-20220414
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://github.com/json-c/json-c/archive/refs/tags/$tarname

description="
The C library for parsing and manipulating strings in JSON format.

JSON-C implements a reference counting object model that allows you to easily
construct JSON objects in C, output them as JSON formatted strings and parse
JSON formatted strings back into the C representation of JSON objects. It aims
to conform to RFC 7159.
"

homepage=https://github.com/json-c/json-c
license=MIT

# Source documentation
docs="COPYING README.md"
docsdir="${docdir}/${program}-${version}"

# The source has a custom directory name
srcdir=${program}-json-c-${version}

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    cmake -DCMAKE_BUILD_TYPE=Release \
     -DCMAKE_C_FLAGS="$QICFLAGS"                                           \
     -DCMAKE_SHARED_LINKER_FLAGS="$QILDFLAGS"                              \
     -DCMAKE_STATIC_LINKER_FLAGS="$QILDFLAGS"                              \
     -DCMAKE_INSTALL_PREFIX=/usr                                           \
     -DCMAKE_INSTALL_LIBDIR=lib${libSuffix}                                \
     -DCMAKE_INSTALL_MANDIR=$mandir                                        \
     -DCMAKE_INSTALL_DOCDIR=$docsdir                                       \
     -DBUILD_SHARED_LIBS=ON                                                \
     -DBUILD_STATIC_LIBS=ON                                                \
     -DBUILD_TESTING=OFF                                                   \
     -DENABLE_THREADING=OFF                                                \
     -G Ninja ..

	#TODO: build docs when Dragora will have doxygen

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    cd ../

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

