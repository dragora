# Build recipe for barelibs (dragora).
#
# Copyright (c) 2021, 2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=barelibs
version=dragora
pkgversion=20230418
release=1

# Define a category for the output of the package name
pkgcategory=libs

description="
Set of various libraries that will be required by many programs.

This package is composed of many (shared) libraries from different
packages, which will be used by many programs.  In case you have not
installed or do not have installed the package containing the required
library.  The part of the shared library will be replaced by the
corresponding software package, once installed.
"

homepage=https://www.dragora.org
license=Custom

build()
{
    rm -rf "$destdir"
    mkdir -p "$destdir"

    cd "$destdir"

    # Read list of file names declared on the 'tracked-names' list.
    #
    # This list is intended to be general, is based on the
    # "Slackware Linux" list(s), which is now a big distribution.
    #
    # It makes sense, since we do not want to be missing a shared
    # library necessary for the execution of some program within
    # the system (or at the beginning of the system installation).
    #
    # Thanks.  :^)
    #
    while read -r name
    do
        for file in $(find /usr/lib${libSuffix} -name "${name}*")
        do
            # We are looking for a file, not for a directory
            test -f "$file" || continue

            # Ignore libtool, static libraries and other extensions
            case "${file##*.}" in
            a | la | pc | spec )
                continue;;
            esac
            if test ! -e "$file"
            then
                printf '%s\n' "" \
                 "WARNING: \`${file##*/}' is not found on the distro installation." \
                 "    This means that the package providing the *shared library* may" \
                 "    not yet be available for the distro, so it will be ignored" \
                 "    for inclusion in the creation of this package." 1>&2
                continue;
            fi

            # Get the real location and real directory of the file

            real_location="$(readlink -- "$file")"
            real_directory="$(dirname -- "$real_location")"

            destination="${real_directory##*@}"
            destination="${destination#*/}"

            # Create directory in destination if needed
            if test ! -d "${destdir}/${destination}"
            then
                mkdir -v -p "${destdir}/${destination}"
            fi

            # Now copy the file as it is (without traverse the path)
            cp -v -f -p -P "$real_location" "${destdir}/${destination}"

            unset -v real_location real_directory destination
        done
    done < "${worktree}"/archive/barelibs/tracked-names

    # Do not include the following (non-essential) libraries

    rm -f \
     "${destdir}/usr/lib${libSuffix}"/libdbus-?-tqt* \
     "${destdir}/usr/lib${libSuffix}"/libdbus-tqt*    
}

