# Build recipe for hwdata.
#
# Copyright (c) 2022-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=hwdata
version=0.367
arch=noarch
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://github.com/vcrhonek/hwdata/archive/v${version}/$tarname

description="
Hardware identification and configuration data.

hwdata contains various hardware identification and configuration data,
such as the pci.ids and usb.ids databases.
"

homepage=https://github.com/vcrhonek/hwdata
license="GPLv2+ | XFree86 1.0 license"

# Source documentation
docs="COPYING LICENSE README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure $configure_args --libdir=/usr/lib${libSuffix}
    make -j${jobs} install DESTDIR="$destdir"

    # Make symlinks of the database for programs looking at /usr/share/
    (
        cd "${destdir}/usr/share" || exit 1
        for file in hwdata/*
        do
            ln -sf "$file" .
        done        
    )

    # Information already provided by the eudev package
    rm -rf "${destdir}/usr/lib${libSuffix}/modprobe.d"
    rmdir "${destdir}/usr/lib${libSuffix}"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

