# Build recipe for libusb-compat.
#
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=libusb-compat
version=0.1.8
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://github.com/libusb/libusb-compat-0.1/releases/download/v${version}/$tarname

description="
USB access library (libusb-1.0 compat wrapper).

A compatibility layer allowing applications written for libusb-0.1 to work
with libusb-1.0. libusb-compat-0.1 attempts to look, feel, smell and walk
like libusb-0.1.
"

homepage=https://github.com/libusb/libusb-compat-0.1
license=LGPLv2.1

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    sh ./bootstrap.sh

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --docdir=$docsdir \
     --enable-shared=yes \
     --enable-static=yes \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

