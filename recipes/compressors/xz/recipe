# Build recipe for xz.
#
# Copyright (c) 2016-2019, 2021-2024 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=xz
version=5.6.2
release=1

# Define a category for the output of the package name
pkgcategory=compressors

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://github.com/tukaani-project/xz/releases/download/v${version}/$tarname

description="
A (complex) compression utility based on the LZMA algorithm.

XZ Utils is free general-purpose data compression software with a high
compression ratio.  XZ Utils were written for POSIX-like systems, but
also work on some not-so-POSIX systems.

WARNING:
  The usage of this utility to distribute files or backup your data
IS TOTALLY DISCOURAGED.  Limit its usage to decompression only,
since some maintainers refuse to distribute in a better format.

For more information, see https://lzip.nongnu.org/xz_inadequate.html

It is highly recommended the use of lzip(1) instead of xz(1).
"

homepage=https://tukaani.org/xz
license=GPLv2+

# Source documentation
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-static=no \
     --enable-shared=yes \
     --disable-rpath \
     --disable-lzmadec \
     --disable-lzmainfo \
     --disable-lzma-links \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi
}

