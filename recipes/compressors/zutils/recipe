# Build recipe for zutils.
#
# Copyright (c) 2017-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=zutils
version=1.12
release=1

# Define a category for the output of the package name
pkgcategory=compressors

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch=https://download.savannah.gnu.org/releases/zutils/$tarname

description="
Utilities dealing with compressed files.

Zutils is a collection of utilities able to process any combination of
compressed and uncompressed files transparently.  If any given file,
including standard input, is compressed, its decompressed content is
used.  Compressed files are decompressed on the fly; no temporary files
are created.

These utilities are not wrapper scripts but safer and more efficient
C++ programs.  In particular the "--recursive" option is very efficient
in those utilities supporting it.
"

homepage=https://zutils.nongnu.org/zutils.html
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CXXFLAGS+="$QICXXFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --infodir=$infodir \
     --mandir=$mandir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # To handle config file
    touch "${destdir}/etc/.graft-config"

    # Compress info documents (if needed)
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

