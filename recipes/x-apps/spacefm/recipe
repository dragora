# Build recipe for spacefm.
#
# Copyright (c) 2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=spacefm
version=20180304_e6f2918
release=2

# Define a category for the output of the package name
pkgcategory=x-apps

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
SpaceFM file manager.

SpaceFM is a multi-panel tabbed file and desktop manager for GNU/Linux
with built-in VFS, udev- or HAL-based device manager, customisable menu
system, and bash-GTK integration. SpaceFM aims to provide a stable,
capable file manager with significant customisation capabilities.
"

homepage=https://ignorantguru.github.io/spacefm/
license="GPLv3+, LGPLv3"

# Source documentation
docs="AUTHORS COPYING* ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

# This build system does not support parallel jobs
jobs=1

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Update for hosts based on musl
    cp -f "${worktree}/archive/common/config.guess" config.guess
    cp -f "${worktree}/archive/common/config.sub" config.sub

    patch -Np1 -i "${worktree}/patches/spacefm/spacefm-include-sysmacros.patch"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # TODO: --enable-video-thumbnails requires FFmpeg

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS -fcommon" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-static=no \
     --enable-shared=yes \
     --disable-video-thumbnails \
     --with-gtk3 \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # To handle (dot) .new files via graft(1)
    touch "${destdir}/etc/spacefm/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

