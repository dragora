# Build recipe for mypaint-brushes.
#
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=mypaint-brushes
version=1.3.1
arch=noarch
release=1

# Define a category for the output of the package name
pkgcategory=x-libs

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=https://github.com/mypaint/mypaint-brushes/releases/download/v${version}/$tarname

description="
MyPaint brushes.

Brushes used by MyPaint and other software using libmypaint.
"

homepage=https://github.com/mypaint/mypaint-brushes
license="CC0 1.0 Universal"

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure $configure_args

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

