# Build recipe for tdebase.
#
# Copyright (c) 2020-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=tdebase
version=20230217_2ffa44200
release=1

# Define a category for the output of the package name
pkgcategory=tde

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
TDE base libraries and programs.

tdebase is the second mandatory package (besides tdelibs) for the
Trinity Desktop Environment.  Here we have various applications
and infrastructure files and libraries.
"

homepage=https://www.trinitydesktop.org/
license="GPLv2+, GFDLv1.2"

# Source documentation
docs="AUTHORS COPYING* README*"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # https://mirror.git.trinitydesktop.org/gitea/TDE/tdebase/pulls/205
    patch -Np1 -i "${worktree}/patches/tdebase/kicker-clock-FTBFS.patch"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    cmake \
     -DCMAKE_EXPORT_COMPILE_COMMANDS=ON                     \
     -DCMAKE_BUILD_TYPE=RelWithDebInfo                      \
     -DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="$QICFLAGS"      \
     -DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="$QICXXFLAGS"  \
     -DCMAKE_SHARED_LINKER_FLAGS:STRING="$(echo $QILDFLAGS | sed 's/-s//')" \
     -DLIB_SUFFIX=${libSuffix}                              \
     -DCMAKE_VERBOSE_MAKEFILE=ON                            \
     -DCMAKE_SKIP_RPATH=OFF                                 \
     -DCMAKE_INSTALL_PREFIX=/opt/trinity                    \
     -DCONFIG_INSTALL_DIR=/etc/trinity                      \
     -DSYSCONF_INSTALL_DIR=/etc/trinity                     \
     -DCMAKE_LIBRARY_PATH=/opt/trinity/lib${libSuffix}      \
     -DCMAKE_INCLUDE_PATH=/opt/trinity/include              \
     -DXDG_MENU_INSTALL_DIR=/etc/xdg/menus                  \
     -DHTDIG_SEARCH_BINARY=/usr/bin/hlsearch                \
     -DWITH_GCC_VISIBILITY=ON                               \
     -DBUILD_ALL=ON                                         \
     -DBUILD_TDM_SYSTEMD_UNIT_FILE=OFF                      \
     -DWITH_USBIDS=/usr/share/hwdata/usb.ids                \
     -DWITH_ARTS=OFF                                        \
     -DWITH_PAM=OFF                                         \
     -DWITH_SASL=OFF                                        \
     -DWITH_ELFICON=ON                                      \
     -DWITH_SUDO_TDESU_BACKEND=ON                           \
     -DWITH_SUDO_KONSOLE_SUPER_USER_COMMAND=OFF             \
     -DWITH_LDAP=OFF                                        \
     -DWITH_SAMBA=OFF                                       \
     -DWITH_LIBUSB=ON                                       \
     -DWITH_LIBRAW1394=OFF                                  \
     -DWITH_OPENEXR=ON                                      \
     -DWITH_XCOMPOSITE=ON                                   \
     -DWITH_XCURSOR=ON                                      \
     -DWITH_XFIXES=ON                                       \
     -DWITH_XRANDR=ON                                       \
     -DWITH_XINERAMA=ON                                     \
     -DWITH_SENSORS=ON                                      \
     -DWITH_HAL=OFF                                         \
     -DWITH_TDEHWLIB=ON                                     \
     -DWITH_XDMCP=ON                                        \
     -DWITH_XRENDER=ON                                      \
     -DWITH_XTEST=ON                                        \
     -DWITH_OPENGL=ON                                       \
     -DWITH_XSCREENSAVER=OFF                                \
     -DWITH_UPOWER=OFF                                      \
     -DWITH_LIBART=ON                                       \
     -DWITH_LIBCONFIG=OFF                                   \
     -DWITH_PCRE=ON                                         \
     -G Ninja ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    # To handle (dot) .new files via graft(1)

    find "${destdir}/etc" -type d -print | while read -r directory
    do
        ( cd -- "$directory" && touch .graft-config )
    done

    # Include wallpaper of our team mate "Oswald Kelso"
    install -p -m 644 "${worktree}/archive/tdebase/bluedragora.jpg" \
     "${destdir}"/opt/trinity/share/wallpapers/

    install -p -m 644 "${worktree}/archive/tdebase/bluedragora.jpg.desktop" \
     "${destdir}"/opt/trinity/share/wallpapers/

    # Set or point to our wallpaper location
    sed \
     -e 's|Trinity-lineart.svg.desktop|bluedragora.jpg.desktop|g' \
     -e 's|Wallpaper=Trinity-lineart.svg|Wallpaper=bluedragora.jpg|g' \
     -i "${destdir}"/opt/trinity/bin/starttde

    # Include xinitrc file to start TDE

    mkdir -p "${destdir}/etc/X11/xinit"
    cp -p "${worktree}/archive/tdebase/xinitrc-tde" \
          "${destdir}/etc/X11/xinit/"
    chmod 644 "${destdir}/etc/X11/xinit/xinitrc-tde"

    # Include profile file

    mkdir -p "${destdir}/etc/profile.d"
    cp -p "${worktree}/archive/tdebase/etc/profile.d/trinity.sh" \
           "${destdir}/etc/profile.d/"
    chmod 644 "${destdir}/etc/profile.d/trinity.sh"

    # To handle .new config file(s)
    touch "${destdir}/etc/profile.d/.graft-config" \
          "${destdir}/etc/X11/xinit/.graft-config"

    # Create post-install script

    mkdir -p "${destdir}/var/lib/qi"
    cat << EOF > "${destdir}/var/lib/qi/${full_pkgname}.sh"

# Make symlink for canonical path at /opt/trinity/lib
#
# Link using \$libSuffix
if test -n "$libSuffix" && test ! -e "\${rootdir}/opt/trinity/lib"
then
    ( cd -- "\${rootdir}/opt/trinity" && ln -sf lib${libSuffix} lib )
fi

echo "Updating desktop database: update-desktop-database -q opt/trinity/share/applications/tde"
chroot "\$rootdir" /usr/bin/update-desktop-database -q opt/trinity/share/applications/tde 2> /dev/null

echo "Updating ICON cache: gtk-update-icon-cache -q -f -i opt/trinity/share/icons/hicolor"
chroot "\$rootdir" /usr/bin/gtk-update-icon-cache -q -f -i opt/trinity/share/icons/hicolor 2> /dev/null

echo "Updating ICON cache: gtk-update-icon-cache -q -f -i opt/trinity/share/icons/crystalsvg"
chroot "\$rootdir" /usr/bin/gtk-update-icon-cache -q -f -i opt/trinity/share/icons/crystalsvg 2> /dev/null

# Set xinitrc as default to start TDE when this package is installed

cd -- "\${rootdir}/etc/X11/xinit" && ln -sf xinitrc-tde xinitrc

EOF

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    cd ..

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

