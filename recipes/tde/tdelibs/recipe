# Build recipe for tdelibs.
#
# Copyright (c) 2020-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=tdelibs
version=20230214_ea10b629
release=1

# Define a category for the output of the package name
pkgcategory=tde

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
The Trinity Desktop Environment (TDE) libraries.

This package includes libraries that are central to the development and
execution of a KDE program, as well as internationalization files for
these libraries, misc HTML documentation, theme modules, and regression
tests.
"

homepage=https://www.trinitydesktop.org/
license="GPLv2+, LGPLv2+ | BSD 2-clause"

# Source documentation
docs="AUTHORS COPYING* DEBUG NAMING README TODO KDE?PORTING.html"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    cmake \
     -DCMAKE_EXPORT_COMPILE_COMMANDS=ON                     \
     -DCMAKE_BUILD_TYPE=RelWithDebInfo                      \
     -DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="$QICFLAGS"      \
     -DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="$QICXXFLAGS"  \
     -DCMAKE_SHARED_LINKER_FLAGS:STRING="$(echo ${QILDFLAGS} | sed 's/-s//')" \
     -DLIB_SUFFIX=${libSuffix}                              \
     -DCMAKE_VERBOSE_MAKEFILE=ON                            \
     -DCMAKE_SKIP_RPATH=OFF                                 \
     -DCMAKE_INSTALL_PREFIX=/opt/trinity                    \
     -DCONFIG_INSTALL_DIR=/etc/trinity                      \
     -DSYSCONF_INSTALL_DIR=/etc/trinity                     \
     -DCMAKE_LIBRARY_PATH=/opt/trinity/lib${libSuffix}      \
     -DCMAKE_INCLUDE_PATH=/opt/trinity/include              \
     -DXDG_MENU_INSTALL_DIR=/etc/xdg/menus                  \
     -DWITH_GCC_VISIBILITY=ON                               \
     -DWITH_CUPS=ON                                         \
     -DWITH_ARTS=OFF                                        \
     -DWITH_LIBART=ON                                       \
     -DWITH_LIBIDN=OFF                                      \
     -DWITH_TIFF=ON                                         \
     -DWITH_JASPER=ON                                       \
     -DWITH_OPENEXR=ON                                      \
     -DWITH_AVAHI=OFF                                       \
     -DWITH_ISPELL=OFF                                      \
     -DWITH_ASPELL=ON                                       \
     -DWITH_HSPELL=OFF                                      \
     -DDEFAULT_SPELL_CHECKER=ASPELL                         \
     -DWITH_ELFICON=ON                                      \
     -DWITH_TDEHWLIB=ON                                     \
     -DWITH_LOGINDPOWER=OFF                                 \
     -DWITH_UDEVIL=ON                                       \
     -DWITH_UPOWER=OFF                                      \
     -DWITH_UDISKS=OFF                                      \
     -DWITH_UDISKS2=OFF                                     \
     -DWITH_CONSOLEKIT=OFF                                  \
     -DWITH_NETWORK_MANAGER_BACKEND=OFF                     \
     -DWITH_LZMA=ON                                         \
     -DWITH_XRANDR=ON                                       \
     -DWITH_XCOMPOSITE=ON                                   \
     -DWITH_MITSHM=ON                                       \
     -DWITH_IMAGETOPS_BINARY=ON                             \
     -G Ninja ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    # This symlink will be handled from the post-install
    # script to point to the correct location
    rm -f "${destdir}/opt/trinity/share/icons/default.tde"

    # To handle (dot) .new files via graft(1)

    find "${destdir}/etc" -type d -print | while read -r directory
    do
        ( cd -- "$directory" && touch .graft-config )
    done

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    cd ..

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

