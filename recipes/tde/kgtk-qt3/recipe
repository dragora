# Build recipe for kgtk-qt3.
#
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=kgtk-qt3
version=20221214_4c52a89
release=1

# Define a category for the output of the package name
pkgcategory=tde

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
TDE dialogs in GTK 2.x applications.

KGtk is a quick-n-dirty hack to allow some Gtk2, Qt3, and Qt4
applications to use KDE3 or KDE4 file dialogs.

KGtk is composed of the following pieces:

1. An application called kdialogd. In this archive there are
   two varieties of this - a KDE3 version, and a KDE4 version.
2. LD_PRELOAD libraries that are used to override the Gtk2, Qt3,
   and Qt4 file dialogs.
"

homepage=https://www.trinitydesktop.org/
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING ChangeLog README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    cmake \
     -DCMAKE_EXPORT_COMPILE_COMMANDS=ON                     \
     -DCMAKE_BUILD_TYPE=RelWithDebInfo                      \
     -DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="$QICFLAGS"      \
     -DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="$QICXXFLAGS"  \
     -DCMAKE_SHARED_LINKER_FLAGS:STRING="$(echo $QILDFLAGS | sed 's/-s//')" \
     -DCMAKE_INSTALL_PREFIX=/opt/trinity                    \
     -DLIB_SUFFIX=${libSuffix}                              \
     -DCMAKE_VERBOSE_MAKEFILE=ON                            \
     -DCMAKE_SKIP_RPATH=OFF                                 \
     -G Ninja ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    cd ..

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

