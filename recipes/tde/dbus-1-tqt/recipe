# Build recipe for dbus-1-tqt.
#
# Copyright (c) 2020-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=dbus-1-tqt
version=20220620_aeb7010
release=1

# Define a category for the output of the package name
pkgcategory=tde

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
A backport of Harald Fernengel's Qt4 D-Bus bindings.

Some Qt4 based code had no direct equivalent in Qt3. Cases where this
transition to Qt3 might not be fully correct are marked with FIXME-QT4

Depending on the use case it might be necessary to extend QDBusMesssage
to allow marshalling/de-marshalling based on an application provided
helper as the Qt3 QVariant can not handle all cases the Qt4 pendant can
(user defined types, etc).
"

homepage=https://www.trinitydesktop.org/
license="Academic Free License version 2.1 | MIT Expat Variant, GPLv2+"

# Source documentation
docs="AUTHORS COPYING* ChangeLog NEWS README TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    cmake \
     -DCMAKE_EXPORT_COMPILE_COMMANDS=ON                     \
     -DCMAKE_BUILD_TYPE=RelWithDebInfo                      \
     -DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="$QICXXFLAGS"  \
     -DCMAKE_SHARED_LINKER_FLAGS:STRING="$(echo $QILDFLAGS | sed 's/-s//')" \
     -DCMAKE_INSTALL_PREFIX=/usr                            \
     -DLIB_SUFFIX=${libSuffix}                              \
     -DCMAKE_VERBOSE_MAKEFILE=ON                            \
     -DWITH_ALL_OPTIONS=ON                                  \
     -G Ninja ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    cd ..

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

