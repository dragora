# Build recipe for /etc.
#
# Copyright (c) 2017-2021, 2023-2024 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=etc
version=dragora
pkgversion=20240808
arch=noarch
release=1

# Define a category for the output of the package name
pkgcategory=data

description="
Essential configuration files.

It contains the essential configuration files for the system.
"

homepage=https://www.dragora.org
license="Apache License version 2"

build()
{
    mkdir -p "${destdir}/etc"
    cp -RPp "${worktree}"/archive/etc/* "${destdir}/etc/"

    # ** This is provided by the bootscripts package **
    rm -rf "${destdir}/etc/rc.d/" \
           "${destdir}/etc/rc.conf" \
           "${destdir}/etc/inittab"

    touch "${destdir}/etc/.graft-config" \
          "${destdir}/etc/skel/.graft-config" \
          "${destdir}/etc/sysctl.d/.graft-config"
}

