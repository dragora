# Build recipe for os-prober.
#
# Copyright (c) 2019, 2021 Matias Fonzo, <selk@dragora.org>.
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=os-prober
version=1.81
release=1

# Define a category for the output of the package name
pkgcategory=boot

tarname=${program}_${version}.tar.xz

# Remote source(s)
fetch=https://deb.debian.org/debian/pool/main/o/os-prober/$tarname

description="
Utility to detect other OSes on a set of drives.

This package detects other OSes available on a system and outputs
the results in a generic machine-readable format.
"

homepage=https://joeyh.name/code/os-prober/
license=GPLv2+

# Source documentation
docs="README TODO debian/changelog debian/copyright"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Import patches from Fedora to make os-prober more reliable
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-no-dummy-mach-kernel.patch"

    # Sent upstream
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-mdraidfix.patch"
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-btrfsfix.patch"
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-bootpart-name-fix.patch"
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-mounted-partitions-fix.patch"
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-factor-out-logger.patch"

    # To be sent upstream
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-factored-logger-efi-fix.patch"
    patch -Np1 -i "${worktree}/patches/os-prober/os-prober-umount-fix.patch"

    # Use 'libsuffix' if defined
    if test -n "$libSuffix"
    then
        find . -type f -exec sed -i -e "s|usr/lib|usr/lib${libSuffix}|g" {} \;
    fi

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    make -j${jobs} V=1 CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS -static"

    mkdir -p "${destdir}/usr/bin"
    cp -p linux-boot-prober "${destdir}/usr/bin"
    cp -p os-prober "${destdir}/usr/bin"
    chmod 755 "${destdir}/usr/bin"/*

    mkdir -p "${destdir}/usr/lib${libSuffix}/os-prober"
    cp -p newns "${destdir}/usr/lib${libSuffix}/os-prober"
    chmod 755 "${destdir}/usr/lib${libSuffix}/os-prober/newns"

    # linux-boot-probes

    mkdir -p "${destdir}/usr/lib${libSuffix}/linux-boot-probes/mounted"

    cd linux-boot-probes
    for file in mounted/common/* mounted/x86/*
    do
        test -f "$file" || continue

        echo "Copying linux-boot-probes/${file} ..."

        cp -p $file \
         "${destdir}/usr/lib${libSuffix}/linux-boot-probes/mounted/"

        chmod 755 \
         "${destdir}/usr/lib${libSuffix}/linux-boot-probes/mounted/${file##*/}"
    done
    unset -v file

    cp -p common/50mounted-tests \
     "${destdir}/usr/lib${libSuffix}/linux-boot-probes"
    chmod 755 \
     "${destdir}/usr/lib${libSuffix}/linux-boot-probes/50mounted-tests"

    cd ..
    # End of linux-boot-probes

    # os-probes

    mkdir -p "${destdir}/usr/lib${libSuffix}/os-probes/mounted/efi"
    cd os-probes
    for file in mounted/common/* mounted/x86/*
    do
        test -f "$file" || continue

        echo "Copying os-probes/${file} ..."

        cp -p $file \
         "${destdir}/usr/lib${libSuffix}/os-probes/mounted/"

        chmod 755 \
         "${destdir}/usr/lib${libSuffix}/os-probes/mounted/${file##*/}"
    done
    unset -v file

    cp -p mounted/x86/efi/* \
     "${destdir}/usr/lib${libSuffix}/os-probes/mounted/efi"

    mkdir -p "${destdir}/usr/lib${libSuffix}/os-probes/init"
    cp -p init/common/* "${destdir}/usr/lib${libSuffix}/os-probes/init"

    cp -p common/50mounted-tests \
      "${destdir}/usr/lib${libSuffix}/os-probes"
    chmod 755 \
     "${destdir}/usr/lib${libSuffix}/os-probes/50mounted-tests"

    cp -p mounted/powerpc/20macosx \
     "${destdir}/usr/lib${libSuffix}/os-probes/mounted"
    chmod 755 "${destdir}/usr/lib${libSuffix}/os-probes/mounted/20macosx"

    cd ..
    # End of os-probes

    mkdir -p "${destdir}/usr/share/os-prober"
    cp -p common.sh "${destdir}/usr/share/os-prober"
    chmod 755 "${destdir}/usr/share/os-prober/common.sh"

    # Create local state directory
    mkdir -p "${destdir}/var/lib/os-prober"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

