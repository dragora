# Build recipe for nftables.
#
# Copyright (c) 2017-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=nftables
version=1.0.6
release=1

# Define a category for the output of the package name
pkgcategory=networking

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=http://ftp.netfilter.org/pub/nftables/$tarname

description="
nftables replaces the popular {ip,ip6,arp,eb}tables.

This software provides a new in-kernel packet classification framework
that is based on a network-specific Virtual Machine (VM) and a new nft
userspace command line tool.  nftables reuses the existing Netfilter
subsystems such as the existing hook infrastructure, the connection
tracking system, NAT, userspace queueing and logging subsystem.
"

homepage=https://netfilter.org/projects/nftables/
license="GPLv2 only"

# Source documentation
docs=COPYING
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --disable-debug \
     --enable-python \
     --with-xtables \
     --with-cli=readline \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

