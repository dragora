# Build recipe for lpppd (a fork of ppp).
#
# Copyright (c) 2019-2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=lpppd
version=20210106_7454257
release=1

# Define a category for the output of the package name
pkgcategory=networking

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
PPP daemon and associated utilities.

The lpppd package is a fork based on Paul Mackerras' PPP:

  Targets glibc, musl libc and uClibc.
  Collecting patches from various sources.
  Use CMake build system.
  Support for non-Linux systems removed.
  Legacy protocol and library support removed.
  Some general code modernization.
  Building of plugins not supported at this time.
"

homepage=https://github.com/snickl/lpppd
license="GPLv2+ | Public domain, BSD-style notices"

# Source documentation
docs="CHANGELOG.md FAQ PLUGINS README* SETUP"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    mkdir BUILD
    cd BUILD

    cmake \
     -DCMAKE_C_FLAGS_RELEASE:STRING="$QICFLAGS -fPIC -Wall -fno-strict-aliasing" \
     -DCMAKE_EXE_LINKER_FLAGS:STRING="$QILDFLAGS"  \
     -DCMAKE_INSTALL_PREFIX=/usr                   \
     -DCMAKE_BUILD_TYPE=Release                    \
     -DCMAKE_VERBOSE_MAKEFILE=ON                   \
     -DOPTION_LPPPD_INET6=ON                       \
     -DOPTION_LPPPD_PPPD_FILTER=ON                 \
     -DOPTION_LPPPD_PPPD_MULTILINK=ON              \
     -G Ninja ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    cd ..

    # Include pppd headers to expose rp-pppoe plugins
    mkdir -p "${destdir}/usr/include/pppd"
    for file in pppd/*.h
    do
        cp -p $file "${destdir}/usr/include/pppd"/
        chmod 644 "${destdir}/usr/include/pppd/${file##*/}"
    done
    unset -v file

    mkdir -p "${destdir}/usr/include/net"
    cp -p include/net/ppp_defs.h "${destdir}/usr/include/net"/
    chmod 644  "${destdir}/usr/include/net/ppp_defs.h"

    # Add configuration files
    mkdir -p "${destdir}/etc/ppp/peers"
    for file in etc.ppp/*
    do
        cp -p $file "${destdir}/etc/ppp"/
        chmod 644 "${destdir}/etc/ppp/${file##*/}"
    done
    unset -v file

    chmod 600 "${destdir}"/etc/ppp/*-secrets*
    touch "${destdir}/etc/ppp/.graft-config"

    # Include scripts
    for file in plog poff pon
    do
        cp -p scripts/${file} "${destdir}/usr/sbin"/
        chmod 755 "${destdir}/usr/sbin/$file"
    done
    unset -v file

    # Include manual page for pon
    mkdir -p "${destdir}/${mandir}/man1"
    cp -p scripts/pon.1 "${destdir}/${mandir}/man1"/

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

