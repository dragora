# Build recipe for xtrans.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2019, 2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=xtrans
version=1.4.0
arch=noarch
release=2

# Define a category for the output of the package name
pkgcategory=xorg_lib

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://www.x.org/releases/individual/lib/$tarname

description="
X transport library.

Library to handle network protocol transport in a modular fashion.
"

homepage=https://www.x.org
license="MIT X Consortium"

# Source documentation
docs="AUTHORS COPYING ChangeLog README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --without-xmlto

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

