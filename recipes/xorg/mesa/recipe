# Build recipe for mesa.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2023 Matias Fonzo <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=mesa
version=22.3.7
release=2

# Define a category for the output of the package name
pkgcategory=xorg_mesa

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=https://archive.mesa3d.org/$tarname

description="
Mesa is an OpenGL compatible 3-D graphics library.

The Mesa project began as an open-source implementation of the OpenGL
specification - a system for rendering interactive 3D graphics.
"

homepage=https://www.mesa3d.org/
license=MIT

# Source documentation
docs="CODEOWNERS README.rst VERSION"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # To provide a few xdemos (Thanks to BLFS!)
    patch -p1 < "${worktree}/patches/mesa/mesa-22.2.4-add_xdemos-1.patch"

    # Apply patches to increment compatibility with Musl
    patch -Np1 -i "${worktree}/patches/mesa/musl-endian.patch"
    patch -Np1 -i "${worktree}/patches/mesa/musl.patch"
    patch -Np1 -i "${worktree}/patches/mesa/musl-stacksize.patch"
    patch -Np1 -i "${worktree}/patches/mesa/megadriver-symlinks.patch"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" CXXFLAGS="$QICXXFLAGS" LDFLAGS="$QILDFLAGS" \
    meson setup $configure_args \
     --libdir /usr/lib${libSuffix} \
     --buildtype=release \
     --strip \
     -Ddri-drivers-path=/usr/lib${libSuffix}/dri \
     -Dglx=dri \
     -Dplatforms=x11 \
     -Dgallium-drivers=auto \
     -Dvulkan-drivers="" \
     -Dgallium-xa=enabled \
     -Dlmsensors=enabled \
     -Degl=enabled \
     -Dgles1=enabled \
     -Dgles2=enabled \
     -Ddri3=enabled \
     -Dosmesa=true \
     -Dopengl=true \
     -Dgallium-nine=true \
     -Dllvm=enabled \
     -Dshared-llvm=enabled \
     -Dshared-glapi=enabled \
     -Dvalgrind=disabled \
     -Db_lto=false \
     -Dcpp_std=gnu++14 \
     ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    unset -v MESA_GIT_SHA1_OVERRIDE

    cd ../

    # Handle .new files via graft(1), if any
    if test -d "${destdir}/etc"
    then
        touch "${destdir}/etc/.graft-config"
    fi
    touch "${destdir}/usr/share/drirc.d/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

