# Build recipe for sysklogd.
#
# Copyright (c) 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=sysklogd
version=2.4.4
release=1

# Define a category for the output of the package name
pkgcategory=daemons

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://github.com/troglobit/sysklogd/releases/download/v${version}/$tarname

description="
Logging system.

The sysklogd package contains programs for logging system messages,
such as those given by the kernel when unusual things happen.
"

homepage=https://github.com/troglobit/sysklogd
license="BSD 3-clause"

# Source documentation
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --without-logger \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Set up configuration file

    mkdir -p "${destdir}/etc/syslog.d"
    cp -p "${worktree}/archive/sysklogd/syslog.conf" "${destdir}/etc"
    chmod 644 "${destdir}/etc/syslog.conf"

    # Set up file for logrotate

    mkdir -p "${destdir}/etc/logrotate.d"
    cp -p "${worktree}/archive/sysklogd/syslogd.logrotate" \
          "${destdir}/etc/logrotate.d"
    chmod 644 "${destdir}/etc/logrotate.d/syslogd.logrotate"

    # Install inetd perp service

    mkdir -p "${destdir}/etc/perp/syslogd"

    cp -p "${worktree}/archive/sysklogd/rc.log" \
          "${worktree}/archive/sysklogd/rc.main" \
          "${destdir}/etc/perp/syslogd/"

    chmod 755 "${destdir}"/etc/perp/syslogd/rc.*

    # THIS SERVICE IS ENABLED BY DEFAULT
    chmod +t "${destdir}/etc/perp/syslogd"

    # To handle dot .new file(s)
    touch "${destdir}/etc/.graft-config" \
          "${destdir}/etc/logrotate.d/.graft-config" \
          "${destdir}/etc/perp/syslogd/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi
}

