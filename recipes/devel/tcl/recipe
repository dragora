# Build recipe for tcl.
#
# Copyright (c) 2018, 2020-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=tcl
version=8.6.13
series_version="${version%.*}"
release=1

# Define a category for the output of the package name
pkgcategory=devel

tarname=${program}${version}-src.tar.gz

# Remote source(s)
fetch=https://prdownloads.sourceforge.net/tcl/$tarname

homepage=https://www.tcl.tk

description="
Tool command language.

Tcl (Tool Command Language) is a very powerful but easy to learn
dynamic programming language, suitable for a very wide range of uses,
including web and desktop applications, networking, administration,
testing and many more.  Open source and business-friendly, Tcl is a
mature yet evolving language that is truly cross platform, easily
deployed and highly extensible.

For more information, visit:
$homepage
"

license=Custom

# Source documentation
docs="ChangeLog README.md changes license.terms"
docsdir="${docdir}/${program}-${version}"

# Custom source directory
srcdir=${program}${version}

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    WD="$(pwd)"

    # Determine whether to use 64 bit support
    case $arch in
    amd64 | x32 )
        bit_flags=--enable-64bit
        ;;
    esac

    cd unix

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" \
    LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --enable-shared \
     --enable-threads \
     --enable-man-symlinks \
     --without-tzdata \
     --build="$(gcc -dumpmachine)" \
     $bit_flags

    unset -v bit_flags
    make -j${jobs} V=1

    # Current versions provided on this release
    itcl_version=4.2.3
    tdbc_version=1.1.5

    # Fix header and library paths
    sed -e "s@^\(TCL_SRC_DIR='\).*@\1/usr/include'@" \
        -e "/TCL_B/s@='\(-L\)\?.*unix@='\1/usr/lib${libSuffix}@" \
        -i tclConfig.sh

    sed -e "s#${WD}/unix/pkgs/itcl${itcl_version}#/usr/lib${libSuffix}/itcl${itcl_version}#"      \
        -e "s#${WD}/pkgs/itcl${itcl_version}/generic#/usr/include#"                               \
        -e "s#${WD}/pkgs/itcl${itcl_version}#/usr/include#"                                       \
        -i pkgs/itcl${itcl_version}/itclConfig.sh

    sed -e "s#${WD}/unix/pkgs/tdbc${tdbc_version}#/usr/lib${libSuffix}/tdbc${tdbc_version}#"      \
        -e "s#${WD}/pkgs/tdbc${tdbc_version}/generic#/usr/include#"                               \
        -e "s#${WD}/pkgs/tdbc${tdbc_version}/library#/usr/lib${libSuffix}/tcl${series_version}#"  \
        -e "s#${WD}/pkgs/tdbc${tdbc_version}#/usr/include#"                                       \
        -i pkgs/tdbc${tdbc_version}/tdbcConfig.sh

    unset -v WD tdbc_version itcl_version

    make -j${jobs} DESTDIR="$destdir" install
    make -j${jobs} INSTALL_ROOT="$destdir" install-private-headers

    # Make symlink(s) for compatibility
    ln -s tclsh${series_version} "${destdir}/usr/bin/tclsh"
    ln -s libtcl${series_version}.so "${destdir}/usr/lib${libSuffix}/libtcl.so"
    ln -s libtclstub${series_version}.a "${destdir}/usr/lib${libSuffix}/libtclstub.a"

    unset -v series_version

    cd ..

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip --force -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

