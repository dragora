# Build recipe for nasm.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (c) 2018, 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=nasm
version=2.15.05
release=1

# Define a category for the output of the package name
pkgcategory=devel

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://www.nasm.us/pub/nasm/releasebuilds/${version}/$tarname

description="
A free portable assembler.

Designed for the Intel 80x86 microprocessor series, using primarily
the traditional Intel instruction mnemonics and syntax.
"

homepage=https://nasm.us
license=BSD

docs="AUTHORS CHANGES ChangeLog LICENSE README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --mandir=$mandir \
     --docdir=$docsdir \
     --disable-pdf-compression \
     --enable-sections \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install install_rdf

    # Compress manpages
    lzip -9 "${destdir}/${mandir}/"man1/*.1

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

