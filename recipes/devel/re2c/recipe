# Build recipe for re2c.
#
# Copyright (c) 2018-2019, 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=re2c
version=3.0
release=1

# Define a category for the output of the package name
pkgcategory=devel

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch=https://github.com/skvadrik/re2c/releases/download/${version}/$tarname

description="
A fast lexer generator for C and C++.

re2c is a free and open-source lexer generator for C and C++.  Its main
goal is generating fast lexers: at least as fast as their reasonably
optimized hand-coded counterparts.  Instead of using traditional
table-driven approach, re2c encodes the generated finite state automata
directly in the form of conditional jumps and comparisons.
"

homepage=https://www.re2c.org
license="Public domain"

# Source documentation
docs="CHANGELOG NO_WARRANTY README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" CXXFLAGS="$QICXXFLAGS" \
     LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1 bootstrap
    make -j${jobs} DESTDIR="$destdir" install

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

