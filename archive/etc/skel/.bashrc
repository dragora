#
# .bashrc - Individual per-interactive-shell startup file (GNU Bash).
#

# Set primary prompt
PS1='\u@\h:\w\$ '

# Secondary prompt string
PS2='> '

# Extended pattern matching language
shopt -s extglob

