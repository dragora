# This file is part of the 'dragora-installer'.
#
# Purpose: Installation of .tlz packages from series.

# Show menu

dialog --colors \
 --backtitle "\ZbPackage installation" \
 --title "SELECT THE INSTALLATION MODE" \
 --no-cancel --menu "" 6 58 0 \
"All"     "Install all packages from chosen series" \
"Expert"  "Select packages individually" \
 2> ${SUBTMPDIR}/return-InstallPackages

# Check menu options

case "$(cat -- ${SUBTMPDIR}/return-InstallPackages)" in
All )
    # Clean for subsequent filling
    : > ${SUBTMPDIR}/return-InstallPackages_All

    # Read (selected) series
    IFS= read -r REPLY < ${SUBTMPDIR}/return-ShowSeries

    for serie in $REPLY
    do
        find -- "${MEDIUM_SOURCE}"/@ARCH@ "${MEDIUM_SOURCE}"/noarch \
         -type f \( -name "*@${serie}.tlz" -a ! -name "*-pass*" \) \
          -printf '%p\n' >> ${SUBTMPDIR}/return-InstallPackages_All \
           2> /dev/null || true
    done
    unset -v REPLY serie

    # Proceed to install the packages from selected series

    if test -s ${SUBTMPDIR}/return-InstallPackages_All
    then
       sort ${SUBTMPDIR}/return-InstallPackages_All \
        > ${SUBTMPDIR}/return-InstallPackages_All.sorted

        # Set some packages at the top of the list to give priority

        count=0
        total=4
        while test $count -lt $total
        do
            for string in hierarchyfs etc 'musl@libs' barelibs
            do
                ed -l -s ${SUBTMPDIR}/return-InstallPackages_All.sorted << EOF
/${string}/m${count}
w
EOF
            count=$((count+1))
            done
        done
        unset -v count total string

        echo ""

        # Calculate total number of packages installed to reflect
        # the progress during package installation

        x=1
        total="$(awk 'END{ print NR }' ${SUBTMPDIR}/return-InstallPackages_All.sorted)"

        while read -r package
        do
            qi install --no-rc \
             --rootdir=/media/dragora-root \
             --packagedir=/usr/pkg \
             --targetdir=/ \
             --prune "$package"

            printf "%${COLUMNS}s %s\n" \
             "$(tput bold)$(tput setaf 3)^ ${x} / ${total}$(tput sgr0)"

            x=$((x+1));		# Increment counter.
        done < ${SUBTMPDIR}/return-InstallPackages_All.sorted

        unset -v x total
    else
        echo "${PROGRAM}: All: Error the package list is empty." 1>&2
        exit 99;
    fi
    ;;
Expert )
    # Clean for subsequent filling
    : > ${SUBTMPDIR}/search-InstallPackages_Expert

    # Read (selected) series
    IFS= read -r REPLY < ${SUBTMPDIR}/return-ShowSeries

    for serie in $REPLY
    do
        find -- "${MEDIUM_SOURCE}"/@ARCH@ "${MEDIUM_SOURCE}"/noarch \
         -type f \( -name "*@${serie}.tlz" -a ! -name "*-pass*" \) \
          -printf '%p\n' >> ${SUBTMPDIR}/search-InstallPackages_Expert \
           2> /dev/null || true
    done
    unset -v REPLY serie

    if test -s ${SUBTMPDIR}/search-InstallPackages_Expert
    then
       sort ${SUBTMPDIR}/search-InstallPackages_Expert \
        > ${SUBTMPDIR}/search-InstallPackages_Expert.sorted
    else
        echo "${PROGRAM}: Expert: The package list is empty." 1>&2
        exit 99;
    fi

    # Dialog, package checklist composition

    cat << EOF > ${SUBTMPDIR}/InstallPackages_Expert
dialog --colors --no-shadow \\
 --backtitle "\ZbPackage selection from chosen series" \\
 --title "SOFTWARE PACKAGES" \\
 --no-cancel --item-help --checklist \\
"The following packages have been found according to the series.\n\n\\
Please use the cursor keys and \\Z3[Space]\\Zn to deselect the packages\n\\
you do not want to install.  By default, all packages are selected." \\
$(( LINES - 6 )) $COLUMNS 16 \\
EOF

    # Populate checklist item using information from meta files
    while read -r meta
    do
        if ! . "${meta}.txt"
        then
            echo "${PROGRAM}: Error no meta file(s) available." 1>&2
            exit 99;
        fi

        echo "\"${full_pkgname%@*}\" \"@${pkgcategory}\" ON \"${blurb}\" \\" \
         >> ${SUBTMPDIR}/InstallPackages_Expert
    done < ${SUBTMPDIR}/search-InstallPackages_Expert.sorted

    # Add tail to get the answers
    echo ' 2> ${SUBTMPDIR}/return-InstallPackages_Expert' \
     >> ${SUBTMPDIR}/InstallPackages_Expert

    touch ${SUBTMPDIR}/return-InstallPackages_Expert

    # Show the menu
    . ${SUBTMPDIR}/InstallPackages_Expert

    # Proceed to install the chosen packages

    if test -s ${SUBTMPDIR}/return-InstallPackages_Expert
    then
       tr ' ' '\n' < ${SUBTMPDIR}/return-InstallPackages_Expert | \
        sort > ${SUBTMPDIR}/return-InstallPackages_Expert.sorted

        # Set some packages at the top of the list to give priority

        count=0
        total=4
        while test $count -lt $total
        do
            for string in hierarchyfs etc 'musl$' barelibs
            do
                ed -l -s ${SUBTMPDIR}/return-InstallPackages_Expert.sorted << EOF
/${string}/m${count}
w
EOF
            count=$((count+1))
            done
        done
        unset -v count total string
    else
        echo "${PROGRAM}: Expert: Error the package list is empty." 1>&2
        exit 99;
    fi

    echo ""

    # Calculate total number of packages installed to reflect
    # the progress during package installation

    x=1
    total="$(awk 'END{ print NR }' ${SUBTMPDIR}/return-InstallPackages_Expert.sorted)"

    # Proceed to install if there is a match in the search list
    while read -r line
    do
        grep -s "${line}[@]" \
         ${SUBTMPDIR}/search-InstallPackages_Expert.sorted | \
          qi install --no-rc \
           --rootdir=/media/dragora-root \
           --packagedir=/usr/pkg \
           --targetdir=/ \
           --prune -

        printf "%${COLUMNS}s %s\n" \
         "$(tput bold)$(tput setaf 3)^ ${x} / ${total}$(tput sgr0)"

        x=$((x+1));		# Increment counter.
    done < ${SUBTMPDIR}/return-InstallPackages_Expert.sorted

    unset -v x total
    ;;
esac

