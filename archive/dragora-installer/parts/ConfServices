# This file is part of the 'dragora-installer'.
#
# Purpose: Show detected services to activate/deactivate.

addEntry()
{
    service=$1
    description=$2

    if test -s "/media/dragora-root/etc/perp/${service}/rc.main"
    then
        # Detect current changes for previous installations

        sticky_bit=$(find /media/dragora-root/etc/perp -name "$service" -perm -1000 -print)

        if test -n "$sticky_bit"
        then
            _toggle_flag=on
        else
            _toggle_flag=off
        fi

        echo "\"${service}\" \"\" $_toggle_flag \"${description}\" \\" \
         >> "${SUBTMPDIR}/ConfServices"
    fi

    unset -v service description sticky_bit _toggle_flag
}

cat << EOF > "${SUBTMPDIR}/ConfServices"
dialog --colors \\
 --backtitle "\\ZbChoosing the running services" \\
 --title "PERSISTENT PROCESS ON BOOT" --no-cancel \\
 --item-help --checklist \\
"A \"persistent process\" is any program intended to be \\
long-running, highly available, and purpose critical. \\
Also known and often described as a \"service\", a persistent \\
process normally provides some essential, on-demand system \\
service.\\n\\n\\
Please select the daemons that you want to start, monitor, log,\\n\\
and control as persistent processes from system startup." $(( LINES - 18 )) 72 0 \\
EOF

addEntry acpid "Advanced Configuration and Power Interface event daemon."
addEntry connmand "A daemon for managing Internet connections."
addEntry crond "Cron daemon. [required]"
addEntry cups-browsed "Remote CUPS printers (If selected, you must also select 'cupsd')."
addEntry cupsd "CUPS scheduler."
addEntry dbus "D-Bus daemon."
addEntry devmon "Device monitor, udevil."
addEntry dhcpcd "DHCP client/daemon (If selected, deselect 'connmand' to avoid conflicts)."
addEntry inetd "Internet super-server."
addEntry snmpd "A daemon to respond to SNMP request packets."
addEntry sshd "The OpenSSH (SSH) daemon."
addEntry syslogd "Log systems messages. [required]"
addEntry udevd "Device event managing daemon. [required]"

unset -f addEntry

echo ' 2> "${SUBTMPDIR}/return-ConfServices"' >> "${SUBTMPDIR}/ConfServices"

# Show the menu

. "${SUBTMPDIR}/ConfServices"
test -s "${SUBTMPDIR}/return-ConfServices" || return 0;

# Set sticky bit according to the selection
for service in acpid connmand crond cups-browsed cupsd dbus devmon dhcpcd \
               inetd snmpd sshd syslogd udevd ; \
do
    if grep -q -o -m 1 "$service" "${SUBTMPDIR}/return-ConfServices"
    then
        chmod a+t /media/dragora-root/etc/perp/${service}
    else
        chmod a-t /media/dragora-root/etc/perp/${service}
    fi
done
unset -v service

