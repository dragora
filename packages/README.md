## packages/ - Contains software packages.

This directory contains the software packages distributed for the official
architectures supported (and provided) by Dragora.  Binary packages that
are generated and updated through the Qi package manager.

